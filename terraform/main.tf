##########
### S3 ###
##########
resource "aws_s3_bucket" "lambda_bucket" {
  bucket        = "lambda-code-maintainer"
  force_destroy = true
}

# block all public access
resource "aws_s3_bucket_public_access_block" "lambda_bucket" {
  bucket = aws_s3_bucket.lambda_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#####################
### API GW lambda ###
#####################

# api gateway lambda IAM role
resource "aws_iam_role" "api_gw_lambda_exec" {
  name = "api-gw-lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

# IAM policy for api gw lambda
resource "aws_iam_policy" "api_gw_lambda_iam_policy" {
  name        = "lambda_dynamodb_policy"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "dynamodb:Attributes",
        "dynamodb:EnclosingOperation",
        "dynamodb:LeadingKeys",
        "dynamodb:ReturnConsumedCapacity",
        "dynamodb:Select",
        "dynamodb:ReturnValues",
        "dynamodb:GetItem",
        "dynamodb:PutItem",
        "dynamodb:BatchGetItem",
        "dynamodb:Query",
        "dynamodb:Scan",
        "dynamodb:BatchWriteItem",
        "dynamodb:UpdateItem",
        "dynamodb:DeleteItem"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Policy attachment
resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
  policy_arn = aws_iam_policy.api_gw_lambda_iam_policy.arn
  role       = aws_iam_role.api_gw_lambda_exec.name
}

# Generates archive for lambda function
# ${path.module} is the current directory
data "archive_file" "api_gw_lambda_zip" {
  type        = "zip"
  source_dir  = "../${path.module}/lambdas/api_gw_lambda"
  output_path = "../${path.module}/lambdas/api_gw_lambda/api_gw_lambda.zip"
}

# upload the zip archive to S3 bucket
resource "aws_s3_object" "lambda_api_gw" {
  bucket = aws_s3_bucket.lambda_bucket.id

  key = "api_gw_lambda.zip"

  source = data.archive_file.api_gw_lambda_zip.output_path
  # etag triggers updates when the value changes
  etag   = filemd5(data.archive_file.api_gw_lambda_zip.output_path)
}

# api gw lambda definition
resource "aws_lambda_function" "api-gw-lambda" {
  # filename      = "${path.module}/lambdas/api_gw_lambda/api_gw_lambda.zip
  function_name = "api_gw_lambda"

  s3_bucket = aws_s3_bucket.lambda_bucket.id
  s3_key = aws_s3_object.lambda_api_gw.key

  role          = aws_iam_role.api_gw_lambda_exec.arn
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.8"
  timeout       = 300
  depends_on    = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
  source_code_hash = data.archive_file.api_gw_lambda_zip.output_base64sha256
}

# Cloudwatch log group
resource "aws_cloudwatch_log_group" "api-gw-log-group" {
  name = "/aws/lambda/${aws_lambda_function.api-gw-lambda.function_name}"
  retention_in_days = 14
}

###################
### Lambda Auth ###
###################

# lambda authorizer lambda IAM role
resource "aws_iam_role" "lambda_authorizer_exec" {
  name = "lambda-authorizer"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

# IAM policy for api gw lambda
resource "aws_iam_policy" "lambda_authorizer_iam_policy" {
  name        = "lambda_cloudwatch_policy"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Policy attachment
resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role_lambda_auth" {
  policy_arn = aws_iam_policy.lambda_authorizer_iam_policy.arn
  role       = aws_iam_role.lambda_authorizer_exec.name
}

# Generates archive for lambda function
# ${path.module} is the current directory
data "archive_file" "lambda_auth_zip" {
  type        = "zip"
  source_dir  = "../${path.module}/lambdas/lambda_authorizer"
  output_path = "../${path.module}/lambdas/lambda_authorizer/lambda_auth.zip"
}

# upload the zip archive to S3 bucket
resource "aws_s3_object" "lambda_auth" {
  bucket = aws_s3_bucket.lambda_bucket.id

  key = "lambda_auth.zip"

  source = data.archive_file.lambda_auth_zip.output_path
  # etag triggers updates when the value changes
  etag   = filemd5(data.archive_file.lambda_auth_zip.output_path)
}

# api gw lambda definition
resource "aws_lambda_function" "lambda-authorizer" {
  #filename      = "${path.module}/lambdas/lambda_authorizer/lambda_auth.zip"
  function_name = "lambda_auth"

  s3_bucket = aws_s3_bucket.lambda_bucket.id
  s3_key = aws_s3_object.lambda_auth.key

  role          = aws_iam_role.lambda_authorizer_exec.arn
  handler       = "function.handler"
  runtime       = "python3.8"
  timeout       = 60
  depends_on    = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role_lambda_auth]
  source_code_hash = data.archive_file.lambda_auth_zip.output_base64sha256
}

# Cloudwatch log group
resource "aws_cloudwatch_log_group" "lambda-auth-log-group" {
  name = "/aws/lambda/${aws_lambda_function.lambda-authorizer.function_name}"
  retention_in_days = 14
}

# Integrating lambda authorizer
resource "aws_apigatewayv2_authorizer" "lambda_auth" {
  api_id                            = aws_apigatewayv2_api.main.id
  authorizer_type                   = "REQUEST"
  authorizer_uri                    = aws_lambda_function.lambda-authorizer.invoke_arn
  identity_sources                  = ["$request.header.authorizationToken"]
  name                              = "lambda_auth"
  authorizer_payload_format_version = "2.0"
}

################
### Dynamodb ###
################

resource "aws_dynamodb_table" "main" {
  name         = "Inventory"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "productId"

  attribute {
    name = "productId"
    type = "S"
  }

}

##############
### API GW ###
##############

# api gw resource
resource "aws_apigatewayv2_api" "main" {
  name          = "main"
  protocol_type = "HTTP"
}

# api gw stage
resource "aws_apigatewayv2_stage" "dev" {
  api_id = aws_apigatewayv2_api.main.id
  name   = "dev"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.main_api_gw.arn

    format          = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

# api gw cloudwatch log group
resource "aws_cloudwatch_log_group" "main_api_gw"{
  name = "/aws/api-gw/${aws_apigatewayv2_api.main.name}"

  retention_in_days = 14
}

# Api gw lambda integration
resource "aws_apigatewayv2_integration" "api_gw_lambda" {
  api_id             = aws_apigatewayv2_api.main.id
  integration_uri    = aws_lambda_function.api-gw-lambda.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

# granting permissions to api gw to invoke api_gw lambda
resource "aws_lambda_permission" "lambda_invoke_permission" {
  statement_id = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api-gw-lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.main.execution_arn}/*/*"
}

# Granting API GW permission to invoke authorizer lambda
resource "aws_lambda_permission" "authorizer" {
  statement_id = "AllowExecutionFromAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda-authorizer.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.main.execution_arn}/*/*"
}

# healthcheck
resource "aws_apigatewayv2_route" "health" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "GET /health"
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

# GET products route
resource "aws_apigatewayv2_route" "GET_products_method" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "GET /products"
  authorization_type = "CUSTOM"
  authorizer_id = aws_apigatewayv2_authorizer.lambda_auth.id
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

# GET products route
resource "aws_apigatewayv2_route" "GET_product_method" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "GET /product"
  authorization_type = "CUSTOM"
  authorizer_id = aws_apigatewayv2_authorizer.lambda_auth.id
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

# POST product route
resource "aws_apigatewayv2_route" "POST_product_method" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "POST /product"
  authorization_type = "CUSTOM"
  authorizer_id = aws_apigatewayv2_authorizer.lambda_auth.id
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

# UPDATE product route
resource "aws_apigatewayv2_route" "UPDATE_product_method" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "PATCH /product"
  authorization_type = "CUSTOM"
  authorizer_id = aws_apigatewayv2_authorizer.lambda_auth.id
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

# DELETE product route
resource "aws_apigatewayv2_route" "DELETE_product_method" {
  api_id    = aws_apigatewayv2_api.main.id
  route_key = "DELETE /product"
  authorization_type = "CUSTOM"
  authorizer_id = aws_apigatewayv2_authorizer.lambda_auth.id
  target    = "integrations/${aws_apigatewayv2_integration.api_gw_lambda.id}"
}

output "path_module" {
  value = path.module
}
