#######################
### Lambda monitors ###
#######################

# Lambda Errors
resource "aws_cloudwatch_metric_alarm" "lambda_error_monitor" {
  alarm_name = "lambda-errors-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "Errors"
  namespace = "AWS/Lambda"
  period = "300"
  statistic = "Sum"
  threshold = "5"
  alarm_description = "Lambda has exceeded the threshold error value in the last 5 mins"

  dimensions = {
    FunctionName = aws_lambda_function.api-gw-lambda.function_name
  }
  alarm_actions = []
}

# Invocations
resource "aws_cloudwatch_metric_alarm" "lambda_invocation_monitor" {
  alarm_name = "lambda-invocation-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "Invocations"
  namespace = "AWS/Lambda"
  period = "300"
  statistic = "Sum"
  threshold = "100"
  alarm_description = "Lambda has exceeded the threshold error value in the last 5 mins"

  dimensions = {
    FunctionName = aws_lambda_function.api-gw-lambda.function_name
  }
  alarm_actions = []
}

################
### DynamoDB ###
################

# Read throttling events
resource "aws_cloudwatch_metric_alarm" "dynamodb_read_throttling_monitor" {
  alarm_name = "dynamodb-read-throttling-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "ConsumedReadCapacityUnits"
  namespace = "AWS/DynamoDB"
  period = "60"
  statistic = "Sum"
  threshold = "50"
  alarm_description = "DynamoDB has exceeded the threshold value for consumed read capacity in the last 1 min"

  dimensions = {
    TableName = aws_dynamodb_table.main.name
  }
  alarm_actions = []
}

# Write throttling events
resource "aws_cloudwatch_metric_alarm" "dynamodb_write_throttling_monitor" {
  alarm_name = "dynamodb-write-throttling-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "ConsumedWriteCapacityUnits"
  namespace = "AWS/DynamoDB"
  period = "60"
  statistic = "Sum"
  threshold = "50"
  alarm_description = "DynamoDB has exceeded the threshold value for consumed read capacity in the last 1 min"

  dimensions = {
    TableName = aws_dynamodb_table.main.name
  }
  alarm_actions = []
}
