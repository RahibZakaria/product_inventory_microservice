terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.21.0"
    }
  }
  backend "http" {}
}

provider "aws" {
  region  = "us-east-1"
}
