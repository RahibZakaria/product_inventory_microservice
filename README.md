# product_inventory_microservice

The goal of this project is to make an end-to-end project involving CI/CD pipeline and deploying AWS services using terraform.
The project is a product inventory microservice, where you can maintain your inventory.

![Infrastructure](/images/diagram.png)

### The following AWS services has been used:
- Lambda
- DynamoDB
- API Gateway
- CloudWatch monitors


### Lambdas
- api gw lambda: Contains business logic for CRUD operations
- lambda authorizer: Authorizes users to use the microservice. Need to pass token in headers as shown below.
![authorization_header](/images/authorization_header.png)

#### Note: Functionality of lambda authorizer can be extended to use 3rd party auth provider like OAuth 2.0


### DynamoDB
- I am using just one column called productId which is both the partition key and primary key in this case.
- DynamoDB has been configured to on-demand meaning you pay per request for the data reads and writes that the application performs on dynamodb


### API gateway resources and methods:

* /health 
    - GET : Returns the health of the microservice. Authentication token in headers is not required

* /products
    - GET : Performs a scan operation on dynamodb and returns all products

* /product
    - GET : Returns single product information. productId with the value needs to be passed as query string parameter.
    - POST : Saves a new product to dynamodb. productId value is mandatory to be passed in the json body when making request.
    - PATCH : Modifies the product information in dynamodb. Request body should contain productId, updateKey (column name) and UpdateValue (new value)
    - DELETE : Deletes the product from dynamodb table. Need to pass productId in request body.

### CloudWatch monitors
- The monitors are created using terraform.
- Currently, the monitors just track if a particular metric has exceeded the threshold limit but no action is taken.
- The functionality can be further extended by triggering AWS SNS service to send notifications or even use lambda to send notification to slack channel.

#### Available monitors
- lambda-errors-alarm: This monitor will alarm you if the number of errors exceeded more than 5 in the last 5 mins
- lambda-invocation-alarm: This monitor will alarm you if the number of invocations have exceeded more than 100 invocations in last 5 mins
- dynamodb-write-throttling-alarm: This monitor will alarm you if the write capacity units have exceeded more than 50 in last 5 mins
- dynamodb-read-throttling-alarm: This monitor will alarm you if the read capacity units have exceeded more than 50 in last 5 mins


### The pipeline contains the following stages:
* test
    - lint test
    - type check using mypy
    - unit-test
* validate terraform code
* plan infrastructure using terraform 
* apply infrastructure using terraform 
* destroy infrastructure (manual trigger)

### Terraform
- The state file is maintained by Gitlab itself and can be found under operate/Terraform states section.

##### Note:
To have this functionality in your project, run the following command locally once before triggering the pipeline.

```
terraform init -reconfigure -backend-config="address=${ADDRESS}" -backend-config="lock_address=${ADDRESS}/lock" -backend-config="unlock_address=${ADDRESS}/lock" -backend-config="username=RahibZakaria" -backend-config="password=$GITLAB_ACCESS_TOKEN" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
```

In the above code you need to replace the placeholder with hard coded values as shown below.

```
terraform init -reconfigure -backend-config="address=https://gitlab.com/api/v4/projects/44589246/terraform/state/my_terraform_state" -backend-config="lock_address=https://gitlab.com/api/v4/projects/44589246/terraform/state/my_terraform_state/lock" -backend-config="unlock_address=https://gitlab.com/api/v4/projects/44589246/terraform/state/my_terraform_state/lock" -backend-config="username=RahibZakaria" -backend-config="password=$GITLAB_ACCESS_TOKEN" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
```

I have not replaced the value for $GITLAB_ACCESS_TOKEN. This should be created by navigating to Settings/Access Tokens.
When creating this token mark all the boxes under Select scopes section and select role as owner.
This token is needed to grant access to your Gitlab repo like read write operation.

The ADDRESS variable has been defined in .gitlab-ci.yml file that looks like the following:

```
ADDRESS: "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${STATE_NAME}"
```

where STATE_NAME can be any name you like and CI_PROJECT_ID can be found as shown below.

![CI_PROJECT_ID](/images/CI_PROJECT_ID.png)

### Environment Variables
You need to create AWS IAM role so that terraform can have access to your AWS account to create resources.
After creating the role copy AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY values and add them in your Gitlab variables that can be found under Settings/ CI/CD / Variables as shown below

![CICD_Variables](/images/CICD_Variables.png)


## Getting started

after cloning the repo, run the following commands:
```
terraform init
terraform apply --auto-approve
```

#### Note:
You need to add AWS ACCESS_KEY and SECRET_ACCESS_KEY in your providers.tf file if you haven't configured it using AWS CLI 

To destroy the infrastructure, run the following command:
```
terraform destroy --auto-approve
```
