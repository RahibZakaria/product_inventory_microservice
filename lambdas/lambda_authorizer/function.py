
def handler(event, context):
    # 1 - Log the event
    print('*********** The event is: ***************')
    print(event)
    print(event['headers'])
    apiId = event['requestContext']['apiId']
    # 2 - See if the person's token is valid
    if event['headers']['authorizationtoken'] == 'abc123':
        auth = 'Allow'
    else:
        auth = 'Deny'

    resourceArn = f'arn:aws:execute-api:us-east-1:492700798506:{apiId}/*/*'
    # 3 - Construct and return the response
    authResponse = {"principalId": "abc123", "policyDocument": {"Version": "2012-10-17", "Statement": [
        {"Action": "execute-api:Invoke",
         "Resource": [resourceArn],
         "Effect": auth}]}}
    return authResponse

# reading material: https://ksarath.medium.com/provisioning-aws-api-gateway-using-terraform-95f64b492397
