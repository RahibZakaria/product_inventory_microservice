import unittest
import boto3
import os
import sys

from moto import mock_dynamodb

src_code_path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, src_code_path)


@mock_dynamodb
class TestDBOperations(unittest.TestCase):

    def setUp(self):
        dynamodb = boto3.resource("dynamodb", region_name="us-east-1")
        dynamodb.create_table(
            TableName="Inventory",
            KeySchema=[
                {"AttributeName": "productId", "KeyType": "HASH"}
            ],
            AttributeDefinitions=[
                {"AttributeName": "productId", "AttributeType": "S"}
            ],
            ProvisionedThroughput={"ReadCapacityUnits": 5, "WriteCapacityUnits": 5},
        )

    def test_saveProduct(self):
        from lambdas.api_gw_lambda.lambda_function import saveProduct
        response = saveProduct({"productId": "1000", "price": 2})
        assert response['statusCode'] == 200

    def test_getProduct(self):
        from lambdas.api_gw_lambda.lambda_function import getProduct
        response = getProduct("1001")
        assert response['statusCode'] == 404

    def test_getProducts(self):
        from lambdas.api_gw_lambda.lambda_function import getProducts
        response = getProducts()
        assert response['statusCode'] == 200

    def test_modifyProduct(self):
        from lambdas.api_gw_lambda.lambda_function import modifyProduct
        response = modifyProduct(productId="1000", updateKey="price", updateValue=4)
        assert response['statusCode'] == 200

    def test_deleteProduct(self):
        from lambdas.api_gw_lambda.lambda_function import deleteProduct
        response = deleteProduct("1000")
        assert response['statusCode'] == 200
