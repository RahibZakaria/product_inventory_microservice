import json
import logging
from db_operations import getProduct, getProducts, saveProduct, deleteProduct, modifyProduct, buildResponse
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    try:

        logger.info(event)
        if event['body']:
            print(json.loads(event['body']))

        if event['queryStringParameters']:
            print(event['queryStringParameters'])

        if event["resource"] == "/health":
            response = buildResponse(body="API is healthy")
        elif event["resource"] == "/products" and event["httpMethod"] == "GET":
            response = getProducts()
        elif event["resource"] == "/product" and event["httpMethod"] == "GET":
            response = getProduct(event['queryStringParameters']['productId'])
        elif event["resource"] == "/product" and event["httpMethod"] == "POST":
            response = saveProduct(json.loads(event['body']))
        elif event["resource"] == "/product" and event["httpMethod"] == "PATCH":
            requestBody = json.loads(event['body'])
            response = modifyProduct(requestBody['productId'], requestBody['updateKey'], requestBody['updateValue'])
        elif event["resource"] == "/product" and event["httpMethod"] == "DELETE":
            requestBody = json.loads(event['body'])
            response = deleteProduct(requestBody['productId'])
        else:
            response = buildResponse(404, "Not Found")

        return response

    except Exception as e:
        return buildResponse(statusCode=500, body=str(e))
